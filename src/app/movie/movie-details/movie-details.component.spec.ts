import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MovieDetailsComponent } from './movie-details.component';
import {RouterModule} from '@angular/router';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {MatFormFieldModule, MatInputModule, MatSelectModule} from '@angular/material';
import {FlexLayoutModule} from '@angular/flex-layout';
import {MovieService} from '../movie.service';
import {CUSTOM_ELEMENTS_SCHEMA} from '@angular/core';
import {of, throwError} from 'rxjs';
import {movieList} from '../../shared/content/movie.mock-data';
import {ERROR_MESSAGE} from '../../shared/constants/error-text';
import {RouterTestingModule} from '@angular/router/testing';

describe('MovieDetailsComponent', () => {
  let component: MovieDetailsComponent;
  let fixture: ComponentFixture<MovieDetailsComponent>;
  let moviesService;

  beforeEach(async(() => {
    moviesService = jasmine.createSpyObj('MovieService', ['getMovieDetailsById']);
    moviesService.getMovieDetailsById.and.returnValue(of(''));

    TestBed.configureTestingModule({
      imports: [RouterTestingModule , BrowserAnimationsModule, MatFormFieldModule, MatInputModule,
        MatSelectModule, FlexLayoutModule],
      declarations: [MovieDetailsComponent],
      providers: [
        {provide: MovieService, useValue: moviesService}
      ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MovieDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  describe('test get movie item data', () => {
    beforeEach(() => {
      component.movieId = 1;
    });

    it('should get movie item data successfully', () => {
      moviesService.getMovieDetailsById.and.returnValue(of(movieList[0]));

      component.ngOnInit();

      expect(component.movieItem).toEqual(movieList[0]);
    });

    it('should show error if getting movie item data was unsuccessful', () => {
      moviesService.getMovieDetailsById.and.returnValue(throwError('error'));

      component.ngOnInit();

      expect(component.error).toEqual(ERROR_MESSAGE);
    });

    afterEach(() => {
      expect(moviesService.getMovieDetailsById).toHaveBeenCalledWith(1);
    });
  });

});
