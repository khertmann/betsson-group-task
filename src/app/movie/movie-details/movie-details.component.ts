import {Component, OnInit} from '@angular/core';
import {MovieService} from '../movie.service';
import {ActivatedRoute} from '@angular/router';
import {MovieItem} from '../../shared/constants/movie.model';
import {ERROR_MESSAGE} from '../../shared/constants/error-text';
import {take} from 'rxjs/operators';

@Component({
  selector: 'app-movie-details',
  templateUrl: './movie-details.component.html',
  styleUrls: ['./movie-details.component.scss']
})
export class MovieDetailsComponent implements OnInit {
  public movieId: number = Number(this.activatedRoute.snapshot.paramMap.get('id'));
  public movieItem: MovieItem;
  public error: string;

  constructor(private activatedRoute: ActivatedRoute, private service: MovieService) { }

  ngOnInit() {
    if (this.movieId) {
      this.getMovieDetailsById();
    }
  }

  private getMovieDetailsById(): void {
    this.service.getMovieDetailsById(this.movieId).pipe(take(1)).subscribe((movieItemData: MovieItem) => {
       this.movieItem = movieItemData;
      }, () => {
        this.error = ERROR_MESSAGE;
      }
    );
  }
}
