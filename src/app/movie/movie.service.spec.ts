import { TestBed } from '@angular/core/testing';

import { MovieService } from './movie.service';
import {MovieItem} from '../shared/constants/movie.model';

describe('MovieService', () => {
  let service: MovieService;
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [MovieService]
    });
    service = TestBed.get(MovieService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should test get all movies', () => {
    service.getMovies().subscribe((result: MovieItem[]) => {
      expect(result).toBeDefined();
    });
  });

  it('should test get all movies', () => {
    service.getMovieDetailsById(1).subscribe((result: MovieItem) => {
      expect(result).toBeDefined();
    });
  });
});
