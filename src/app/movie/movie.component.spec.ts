import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MovieComponent } from './movie.component';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {MatFormFieldModule, MatInputModule, MatSelectModule} from '@angular/material';
import {FlexLayoutModule} from '@angular/flex-layout';
import {CUSTOM_ELEMENTS_SCHEMA} from '@angular/core';
import {MovieService} from './movie.service';
import {of, throwError} from 'rxjs';
import {RouterModule} from '@angular/router';
import {movieList} from '../shared/content/movie.mock-data';
import {ERROR_MESSAGE} from '../shared/constants/error-text';
import {GenreType, genreType} from '../shared/constants/movie.model';
import {RouterTestingModule} from '@angular/router/testing';

describe('MovieComponent', () => {
  let component: MovieComponent;
  let fixture: ComponentFixture<MovieComponent>;
  let moviesService;

  beforeEach(async(() => {
    moviesService = jasmine.createSpyObj('MovieService', ['getMovies']);
    moviesService.getMovies.and.returnValue(of(''));

    TestBed.configureTestingModule({
      imports: [RouterTestingModule , BrowserAnimationsModule, MatFormFieldModule, MatInputModule,
        MatSelectModule, FlexLayoutModule],
      declarations: [MovieComponent],
      providers: [
        {provide: MovieService, useValue: moviesService}
      ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MovieComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
    expect(component.genreList).toEqual(['action', 'adventure', 'biography', 'comedy', 'crime', 'drama', 'history', 'mystery',
      'scifi', 'sport', 'thriller']);
  });

  describe('test get movies list', () => {
    it('should get movies list successfully', () => {
      moviesService.getMovies.and.returnValue(of(movieList));

      component.ngOnInit();

      expect(component.fullMoviesList).toEqual(movieList);
    });

    it('should show error if getting movie list was unsuccessful', () => {
      moviesService.getMovies.and.returnValue(throwError('error'));

      component.ngOnInit();

      expect(component.error).toEqual(ERROR_MESSAGE);
    });

    afterEach(() => {
      expect(moviesService.getMovies).toHaveBeenCalled();
    });
  });

  describe('test filtering data', () => {
    const selectedGenre: GenreType = genreType.sport;
    const searchedMovieTitle: string = movieList[0].name;

    beforeEach(() => {
      component.fullMoviesList = movieList;
    });

    it('should set search text', () => {
      component.searchMovieTitle(searchedMovieTitle);

      expect(component.searchText).toEqual(searchedMovieTitle);
    });

    it('should filter data by movie title', () => {
      component.searchMovieTitle(searchedMovieTitle);

      expect(component.filteredMovies).toEqual([{
        id: 1,
        key: 'deadpool',
        name: 'Deadpool',
        description: 'A former Special Forces operative turned mercenary is subjected to a rogue experiment that ' +
          'leaves him with accelerated healing powers, adopting the alter ego Deadpool.',
        genres: [genreType.action, genreType.adventure, genreType.comedy],
        rate: '8.6',
        length: '1hr 48mins',
        img: 'deadpool.jpg'
      }]);
    });

    it('should show all genres if no filter is picked', () => {
      component.setGenreFilter(selectedGenre);

      expect(component.selectedGenre).toEqual(selectedGenre);
    });

    it('should filter data by genre', () => {
      component.setGenreFilter(selectedGenre);

      expect(component.filteredMovies).toEqual([{
        id: 4,
        key: 'gridiron-gang',
        name: 'Gridiron Gang',
        description: 'Teenagers at a juvenile detention center, under the leadership of their counselor,' +
          'gain self-esteem by playing football together.',
        genres: [genreType.crime, genreType.drama, genreType.sport],
        rate: '6.9',
        length: '2hr 5mins',
        img: 'gridiron-gang.jpg'
      },   {
        id: 12,
        key: 'southpaw',
        name: 'Southpaw',
        description: 'Boxer Billy Hope turns to trainer Tick Wills to help him get his life back on track after losing his wife in a ' +
          'tragic accident and his daughter to child protection services.',
        genres: [genreType.action, genreType.drama, genreType.sport],
        rate: '7.5',
        length: '2hr 4mins',
        img: 'southpaw.jpg'
      }]);
    });
  });
});
