import {Component, OnInit} from '@angular/core';
import {genreType, GenreType, MovieItem} from '../shared/constants/movie.model';
import {MovieService} from './movie.service';
import {ERROR_MESSAGE} from '../shared/constants/error-text';
import {take} from 'rxjs/operators';

@Component({
  selector: 'app-movie',
  templateUrl: './movie.component.html',
  styleUrls: ['./movie.component.scss']
})
export class MovieComponent implements OnInit {
  public fullMoviesList: MovieItem[] = [];
  public genreList: string[] = [];
  public searchText: string;
  public selectedGenre: GenreType;
  public error: string;

  constructor(private service: MovieService) { }

  ngOnInit() {
    this.genreList = Object.keys(genreType).filter(String);
    this.getAllMovies();
  }

  private getAllMovies(): void {
    this.service.getMovies().pipe(take(1)).subscribe((movieListData: MovieItem[]) => {
      this.fullMoviesList = movieListData;
    }, () => {
      this.error = ERROR_MESSAGE;
    }
    );
  }

  get filteredMovies(): MovieItem[] {
    let result: MovieItem[] = [...this.fullMoviesList];

    if (this.searchText) {
      result = result.filter(movieItem => movieItem.name.toLowerCase().includes(this.searchText.toLowerCase()));
    }

    if (this.selectedGenre) {
      result = result.filter(movieItem => movieItem.genres.includes(this.selectedGenre));
    }

    return result;
  }

  searchMovieTitle(searchedText: string): void {
    this.searchText = searchedText;
  }

  setGenreFilter(selectedGenre: GenreType): void {
    this.selectedGenre = selectedGenre;
  }

}
