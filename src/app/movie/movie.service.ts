import { Injectable } from '@angular/core';
import {movieList} from '../shared/content/movie.mock-data';
import {Observable, Subject} from 'rxjs';
import {MovieItem} from '../shared/constants/movie.model';

@Injectable({
  providedIn: 'root'
})
export class MovieService {

  constructor() { }

  getMovies(): Observable<MovieItem[]> {
    return Observable.create( observer => {
      observer.next(movieList);
      observer.complete();
    });
  }

  getMovieDetailsById(movieId: number): Observable<MovieItem> {
    const selectedMovieItem: MovieItem = movieList.find(movieItem => movieItem.id === movieId);
    return Observable.create( observer => {
      observer.next(selectedMovieItem);
      observer.complete();
    });
  }
}
