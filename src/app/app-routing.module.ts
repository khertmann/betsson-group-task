import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {RouterModule, Routes} from '@angular/router';
import {MovieComponent} from './movie/movie.component';
import {MovieDetailsComponent} from './movie/movie-details/movie-details.component';

export const routes: Routes = [
  {
    path: '',
    component: MovieComponent
  },
  {
    path: 'movie/:id',
    component: MovieDetailsComponent,
  },
  {
    path: '**', redirectTo: '', pathMatch: 'full'
  }
];


@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
