## Movie collection application
Homepage features all movie posters, when hovering over poster, poster flips and shows movie title and option to see more details. Movies can be filtered by searching movie name or filtering them by genre or using both. 

## How to run application locally
 
With NPM:
```sh
git clone git@gitlab.com:khertmann/betsson-group-task.git
cd betsson-group-task
npm install
npm start
```
 
With Yarn:
```sh
git clone git@gitlab.com:khertmann/betsson-group-task.git
cd betsson-group-task
yarn
yarn start
```

## Running Tests
 
Note: Tests use Karma and require Google Chrome or Chromium installed and `CHROME_BIN` variable pointing to your Chrome binary.
 
With NPM:
```sh
npm run e2e
npm test
```
 
With Yarn:
```sh
yarn e2e
yarn test
```

## Tehnical
- Angular Material is used for styling
- Flex Layout is used for responsiveness
- Observables are used for data handling
- All functionalities have test coverage
